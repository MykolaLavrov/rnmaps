import React, { Component } from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import MapView, { Marker } from 'react-native-maps';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      latitude: 0,
      longitude: 0,
      latitudeDelta: 0.015,
      longitudeDelta: 0.0121,
    };
    this.watchId = null;
  }

  componentDidMount() {
    const { geolocation: { getCurrentPosition, watchPosition } } = navigator;
    getCurrentPosition(
      ({ coords: { latitude, longitude } }) => this.setState({
        latitude,
        longitude,
      }),
      error => console.warn('error', error),
    );

    if (!this.watchId) {
      this.watchId = watchPosition(
        ({ coords: { latitude, longitude } }) => this.setState({
          latitude,
          longitude,
        }),
        error => console.warn('error', error),
      );
    }
  }

  componentWillUnmount() {
    if (this.watchId) {
      const { geolocation: { clearWatch, stopObserving } } = navigator;
      clearWatch(this.watchId);
      stopObserving();
    }
  }
  render() {
    const {
      latitude,
      longitude,
      latitudeDelta,
      longitudeDelta,
    } = this.state;
    return (
      <View style={styles.container}>
        <MapView
          style={styles.map}
          region={{
            latitude,
            longitude,
            latitudeDelta,
            longitudeDelta,
          }}
        >
          <Marker
            coordinate={{
              latitude,
              longitude,
            }}
            title={'Title'}
            description={'You are here'}
          />
        </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
